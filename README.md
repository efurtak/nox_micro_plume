# NOx micro plume #

NO is injected into a O3 rich atmosphere, and undergoes the reactions:


O3 + NO -> NO2 + O2

O+ O2 -> O3

NO2 -> NO + O



![Alt text](NOX_plume_early.png )

![Alt text](NOX_plume.png )


